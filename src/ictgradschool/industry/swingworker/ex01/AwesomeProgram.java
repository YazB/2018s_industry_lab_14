package ictgradschool.industry.swingworker.ex01;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ExecutionException;

public class AwesomeProgram implements ActionListener {
//    private JLabel progressLabel = …;
//    private JLabel myLabel = …;
//    private JButton myButton = …;

    /**
     * Called when the button is clicked.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
//        myButton.setEnabled(false);
// Start the SwingWorker running
        MySwingWorker worker = new MySwingWorker();
        try {
            worker.doInBackground();
            //Above line should be worker.execute();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
// When the SwingWorker has finished, display the result in
// myLabel.
        int result = 0;
        try {
            result = worker.get();
            //worker.get(); should be private and be in the done method (not shown in excerpt
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e1) {
            e1.printStackTrace();
        }
//        myButton.setEnabled(true);
//        myLabel.setText("Result: " + result);
    }

    private class MySwingWorker extends SwingWorker <Integer, Void> {
        //Have changed int to Integer as it is an object in <>
        protected Integer doInBackground() throws Exception {
            Integer result = 0;
            for (int i = 0; i < 100; i++) {
// Do some long-running stuff
                //result += doStuffAndThings();
// Report intermediate results
//                progressLabel.setText("Progress: " + i + "%");
                //Here, progress label is in wrong place: should be in event dispatch thread, not worker thread!
            }
            return result;
        }
    }
}
