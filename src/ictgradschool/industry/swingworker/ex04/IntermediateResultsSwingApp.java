package ictgradschool.industry.swingworker.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

/**
 * Simple application to calculate the prime factors of a given number N.
 * <p>
 * The application allows the user to enter a value for N, and then calculates
 * and displays the prime factors. This is a very simple Swing application that
 * performs all processing on the Event Dispatch thread.
 */
public class IntermediateResultsSwingApp extends JPanel {
    private PrimeFactorizationWorker pfw;
    private JButton _startBtn;        // Button to start the calculation process.
    private JButton _abortBtn;
    private JTextArea _factorValues;  // Component to display the result.

    public IntermediateResultsSwingApp() {
        // Create the GUI components.
        JLabel lblN = new JLabel("Value N:");
        final JTextField tfN = new JTextField(20);


        _startBtn = new JButton("Compute");
        _abortBtn = new JButton("Abort");
        _factorValues = new JTextArea();
        _factorValues.setEditable(false);

        // Add an ActionListener to the start button. When clicked, the
        // button's handler extracts the value for N entered by the user from
        // the textfield and find N's prime factors.
        _startBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                String strN = tfN.getText().trim();
                long n = 0;

                try {
                    n = Long.parseLong(strN);
                } catch (NumberFormatException e) {
                    System.out.println(e);
                }
                 pfw = new PrimeFactorizationWorker(n);
                pfw.execute();
                //THESE TWO LINES: INITIALISE AND EXECUTE

                // Disable the Start button until the result of the calculation is known.
                _startBtn.setEnabled(false);

                _abortBtn.setEnabled(true);

                // Clear any text (prime factors) from the results area.
                _factorValues.setText(null);

                // Set the cursor to busy.
                setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                //Array list??


                //REMEMBER TO CALL PRIME FACTORIZATION WORKERS BY ACTUAL NAME
            }
        });

        _abortBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pfw.cancel(true);
            }
        });


        // Construct the GUI.
        JPanel controlPanel = new JPanel();
        controlPanel.add(lblN);
        controlPanel.add(tfN);
        controlPanel.add(_startBtn);
        controlPanel.add(_abortBtn);

        JScrollPane scrollPaneForOutput = new JScrollPane();
        scrollPaneForOutput.setViewportView(_factorValues);

        setLayout(new BorderLayout());
        add(controlPanel, BorderLayout.NORTH);
        add(scrollPaneForOutput, BorderLayout.CENTER);
        setPreferredSize(new Dimension(500, 300));
    }

    private class PrimeFactorizationWorker extends SwingWorker <List <Long>, Long> {
        //Here we are making the inner nested class
        private long N;


        public PrimeFactorizationWorker(long N) {
            //passing N as parameter
            this.N = N;
            //Intialising N against itself
        }

        @Override
        protected List <Long> doInBackground() throws Exception {
            List <Long> primeFactors = new ArrayList <>();
            //Made List to store primeFactors
            // TODO: Calculate prime factors
            // Start the computation in the Event Dispatch thread.
            for (long i = 2; i * i <= N; i++) {

                // If i is a factor of N, repeatedly divide it out
                while (N % i == 0 && !isCancelled()) {
                    primeFactors.add(i);
                    publish(i);
                    N = N / i;
                }
            }

            if (N > 1) {
                primeFactors.add(N);
                publish(N);
            }

            return null;
        }

        @Override
        protected void process(List<Long> chunks) {
            for (Long num: chunks) {
                _factorValues.append(num.toString() + "\n");
                //append is add to end
            }
        }

        @Override
        protected void done() {
//            List <Long> factors = null;
//            try {
//                factors = get();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            } catch (ExecutionException e) {
//                e.printStackTrace();
//            }
            // TODO: Update the UI components with the results


            // Re-enable the Start button.
            _startBtn.setEnabled(true);

            _abortBtn.setEnabled(false);

            // Restore the cursor.
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }


    }



    private static void createAndShowGUI() {
        // Create and set up the window.
        JFrame frame = new JFrame("Prime Factorisation of N");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Create and set up the content pane.
        JComponent newContentPane = new IntermediateResultsSwingApp();
        frame.add(newContentPane);

        // Display the window.
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}

